// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "CarWarGameMode.h"
#include "Character/HovercraftPawn.h"
#include "UObject/ConstructorHelpers.h"
#include "GameFramework/PlayerController.h"
#include "Engine/Engine.h"
#include "Sound/SoundBase.h"
#include "Kismet/GameplayStatics.h"

ACarWarGameMode::ACarWarGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprints/BP_Pawn"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}

void ACarWarGameMode::BeginPlay()
{
	// Switch input mode into fame only
	APlayerController* PC = Cast<APlayerController>(GetWorld()->GetFirstPlayerController());
	PC->SetInputMode(FInputModeGameOnly());
	PC->SetPause(false);

	// Play background music and watchers' noise
	UGameplayStatics::PlaySound2D(GetWorld(), NoiseSoundClass);
	UGameplayStatics::PlaySound2D(GetWorld(), BackgroundMusicSoundClass);
}

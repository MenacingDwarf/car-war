#include "EnemyGenerator.h"
#include "Enemies/MasterEnemy.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/Engine.h"
#include "Engine/World.h"
#include "TimerManager.h"
#include "GameFramework/PlayerController.h"
#include "Engine/Engine.h"

#include "Character/HovercraftPlayerState.h"
#include "UI/MainHUD.h"

// Sets default values
AEnemyGenerator::AEnemyGenerator()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Enemy spawning intervals in seconds, this time is reducing every spawning until min spawn time
	SpawnTime = 5.f;
	MinSpawnTime = 2.f;

	// Enemy spawning probability in seconds, this probability is increasing every spawning until max spawn probability
	SpawnProbability = 0.2f;
	MaxSpawnProbability = 0.8f;

	// Every enemy spawning this variable will be increased to count amount of spawned waves
	WaveCount = 0;
	WallsAmount = 4;
}

// Called when the game starts or when spawned
void AEnemyGenerator::BeginPlay()
{
	Super::BeginPlay();
	// Get MainHUD ref for waves count displaying
	MainHUDRef = Cast<AMainHUD>(GetWorld()->GetFirstPlayerController()->GetHUD());
	// Begin periodically enemy spawning
	GetWorld()->GetTimerManager().SetTimer(EnemySpawnTimer, this, &AEnemyGenerator::SpawnEnemies, SpawnTime, false);
}

void AEnemyGenerator::SpawnEnemies()
{
	// Waves Count should be increased and displayed on UI
	UpdateWavesCount();

	// For every spawn point with spawn probability we should spawn new enemy
	for (FSpawnPoint SpawnPoint : SpawnPoints)
	{
		if (UKismetMathLibrary::RandomBoolWithWeight(SpawnProbability))
		{
			SpawnSingleEnemy(SpawnPoint);
		}
	}

	// Reduce spawn interval time and increase spawn probability
	SpawnTime = FMath::Clamp(SpawnTime - 0.125f, MinSpawnTime, 100.f);
	SpawnProbability = FMath::Clamp(SpawnProbability + 0.25f, 0.f, MaxSpawnProbability);

	GetWorld()->GetTimerManager().SetTimer(EnemySpawnTimer, this, &AEnemyGenerator::SpawnEnemies, SpawnTime, false);
}

void AEnemyGenerator::SpawnSingleEnemy(FSpawnPoint SpawnPoint)
{
	// Count enemy location in spawn point and enemy destination for moving to it
	FVector EnemyLocation = GetActorLocation() + SpawnPoint.PointLocation;
	FVector EnemyDestination = GetRandomDestination(SpawnPoint.WallIndex);

	// Create new enemy and start his moving to destination
	AMasterEnemy* NewEnemy = GetWorld()->SpawnActor<AMasterEnemy>(GetRandomEnemyClass(), FTransform(EnemyLocation));
	if (NewEnemy)
	{
		NewEnemy->StartMovement(EnemyDestination);
	}
}

FVector AEnemyGenerator::GetRandomDestination(int ExcludedWallIndex)
{
	// try to get the wall index until the resulting index ceases to match the excluded index
	int WallIndex;
	WallIndex = UKismetMathLibrary::RandomInteger(WallsAmount);
	while (WallIndex == ExcludedWallIndex)
	{
		WallIndex = UKismetMathLibrary::RandomInteger(WallsAmount);
	}

	// Return random point between two wall indexes
	return GetRandomPointOnWall(WallIndex);
}

FVector AEnemyGenerator::GetRandomPointOnWall(int WallIndex)
{
	TArray<FVector> PointsLocations;
	for (auto SpawnPoint : SpawnPoints)
	{
		if (SpawnPoint.WallIndex == WallIndex)
		{
			PointsLocations.Add(SpawnPoint.PointLocation);
		}
	}

	return PointsLocations[FMath::RandRange(0, PointsLocations.Num() - 1)];
}

TSubclassOf<class AMasterEnemy> AEnemyGenerator::GetRandomEnemyClass()
{
	// Get max possible enemy class index. New enemy class should be added to spawn algorithm every 5 waves
	int MaxClass = UKismetMathLibrary::Clamp(WaveCount / 5, 0, EnemyClasses.Num()-1);

	// Every class has own spawn weight which defines the spawn probability of this class
	// First of all summary weight whould be calculated
	int SumSpawnWeight = 0;
	for (int i = 0; i <= MaxClass; i++)
	{
		SumSpawnWeight += EnemyClasses[i].SpawnWeight;
	}

	// Generate random weight and starting from zero sum and zero index
	int RandomSum = FMath::RandRange(1, SumSpawnWeight);
	int CurrentSum = 0;
	int ClassIndex = 0;

	// Add class weight to sum until sum weight less then random sum
	// After this summarizing we will receive random class defined by its spawn weight
	while (CurrentSum < RandomSum)
	{
		CurrentSum += EnemyClasses[ClassIndex].SpawnWeight;
		ClassIndex++;
	}

	// Get random class with found index from classes array and return it
	TSubclassOf<class AMasterEnemy> RandomClass = EnemyClasses[ClassIndex-1].EnemyClass;
	return RandomClass;
}

void AEnemyGenerator::UpdateWavesCount()
{
	// Increase Waves Count in this generator and in player state
	WaveCount++;
	AHovercraftPlayerState *HovercraftPlayerState = GetWorld()->GetFirstPlayerController()->GetPlayerState<AHovercraftPlayerState>();
	if (HovercraftPlayerState)
	{
		HovercraftPlayerState->WavesCount++;
	}

	// Update widget
	MainHUDRef->UpdateWaves(WaveCount);
}

// Called every frame
void AEnemyGenerator::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


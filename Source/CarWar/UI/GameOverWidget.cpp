// Fill out your copyright notice in the Description page of Project Settings.


#include "GameOverWidget.h"
#include "Components/Button.h"
#include "Kismet/GameplayStatics.h"

UGameOverWidget::UGameOverWidget(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer)
{
}

void UGameOverWidget::NativeConstruct()
{
	Super::NativeConstruct();
	RestartButton->OnClicked.AddDynamic(this, &UGameOverWidget::RestartGame);
}

void UGameOverWidget::RestartGame()
{
	UGameplayStatics::OpenLevel(GetWorld(), "MenuMap");
}

void UGameOverWidget::UpdateGameOverInfo(int WavesCount, int Scores)
{
	if (TXTTotalWavesCount && TXTTotalScores)
	{
		TXTTotalWavesCount->SetText(FText::FromString(FString::FromInt(WavesCount)));
		TXTTotalScores->SetText(FText::FromString(FString::FromInt(Scores)));
	}
}

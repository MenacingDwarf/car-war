// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Runtime/UMG/Public/UMG.h"
#include "MenuWidget.generated.h"

/**
 * 
 */
UCLASS()
class CARWAR_API UMenuWidget : public UUserWidget
{
	GENERATED_BODY()

public:

	UMenuWidget(const FObjectInitializer& ObjectInitializer);

	virtual void NativeConstruct() override;

protected:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget, AllowPrivateAccess = "true"))
	UTextBlock* TXTTotalWavesCount;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget, AllowPrivateAccess = "true"))
	UTextBlock* TXTTotalScores;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget, AllowPrivateAccess = "true"))
	UButton* NewGameButton;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget, AllowPrivateAccess = "true"))
	UButton* ExitButton;

	UFUNCTION()
	void StartNewGame();

	UFUNCTION()
	void QuitGame();

	UFUNCTION()
	void LoadRecord();
	
};

// Fill out your copyright notice in the Description page of Project Settings.


#include "MainHUD.h"
#include "Components/WidgetComponent.h"
#include "MainWidget.h"
#include "GameFramework/PlayerController.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/Engine.h"
#include "GameFramework/PlayerController.h"

#include "../Character/HovercraftPlayerState.h"
#include "GameOverWidget.h"

AMainHUD::AMainHUD()
{

}

void AMainHUD::DrawHUD()
{
	Super::DrawHUD();
}

void AMainHUD::BeginPlay()
{
	Super::BeginPlay();
	if (MainWidgetClass)
	{
		// At the beggining main widget should be created and starting amount of projectiles should be displayed on it
		MainWidget = CreateWidget<UMainWidget>(GetWorld(), MainWidgetClass);
		if (MainWidget)
		{
			MainWidget->AddToViewport();
			MainWidget->UpdateProjectiles(30);
		}
	}
}

void AMainHUD::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
}

void AMainHUD::StartGame(int ProjectilesAmount)
{
	
}

void AMainHUD::UpdateProjectiles(int ProjectilesAmount)
{
	if (MainWidget)
	{
		MainWidget->UpdateProjectiles(ProjectilesAmount);
	}
}

void AMainHUD::UpdateScores(int Scores)
{
	if (MainWidget)
	{
		MainWidget->UpdateScores(Scores);
	}
}

void AMainHUD::UpdateWaves(int WavesCount)
{
	if (MainWidget)
	{
		MainWidget->UpdateWaves(WavesCount);
	}
}

void AMainHUD::GameOver()
{
	// Hide Main Widget
	if (MainWidget)
	{
		MainWidget->RemoveFromParent();
	}

	if (GameOverWidgetClass)
	{
		// Create and display Game Over Widget
		GameOverWidget = CreateWidget<UGameOverWidget>(GetWorld(), GameOverWidgetClass);
		if (GameOverWidget)
		{
			GameOverWidget->AddToViewport();

			// Get current result from Player State and display it on widget
			AHovercraftPlayerState *HovercraftPlayerState = GetWorld()->GetFirstPlayerController()->GetPlayerState<AHovercraftPlayerState>();
			if (HovercraftPlayerState)
			{
				GameOverWidget->UpdateGameOverInfo(HovercraftPlayerState->WavesCount, HovercraftPlayerState->Scores);
			}
		}
	}
	
}

// Fill out your copyright notice in the Description page of Project Settings.


#include "MenuWidget.h"
#include "Components/Button.h"
#include "GameFramework/PlayerController.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "../Game/CarWarSaveGame.h"

UMenuWidget::UMenuWidget(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer)
{
}

void UMenuWidget::NativeConstruct()
{
	Super::NativeConstruct();
	NewGameButton->OnClicked.AddDynamic(this, &UMenuWidget::StartNewGame);
	ExitButton->OnClicked.AddDynamic(this, &UMenuWidget::QuitGame);

	// Current record should be loaded form Save Game and displayed on widget
	LoadRecord();
}

void UMenuWidget::StartNewGame()
{
	UGameplayStatics::OpenLevel(GetWorld(), "MainMap");
}

void UMenuWidget::QuitGame()
{
	APlayerController* PC = Cast<APlayerController>(GetWorld()->GetFirstPlayerController());
	UKismetSystemLibrary::QuitGame(GetWorld(), PC, EQuitPreference::Quit, false);
}

void UMenuWidget::LoadRecord()
{
	// Receive scores and waves count from record Save Game and display it on the widget
	UCarWarSaveGame* SaveGameInstance = Cast<UCarWarSaveGame>(UGameplayStatics::LoadGameFromSlot("Record", 0));

	if (SaveGameInstance && TXTTotalWavesCount && TXTTotalScores)
	{
		int Scores = SaveGameInstance->Scores;
		int WavesCount = SaveGameInstance->WavesCount;

		TXTTotalScores->SetText(FText::FromString(FString::FromInt(Scores)));
		TXTTotalWavesCount->SetText(FText::FromString(FString::FromInt(WavesCount)));
	}
}
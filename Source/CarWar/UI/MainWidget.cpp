// Fill out your copyright notice in the Description page of Project Settings.


#include "MainWidget.h"

UMainWidget::UMainWidget(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer)
{
}

void UMainWidget::NativeConstruct()
{
	Super::NativeConstruct();
}

void UMainWidget::UpdateProjectiles(int ProjectilesAmount)
{
	if (TXTCurrentProjectiles)
	{
		TXTCurrentProjectiles->SetText(FText::FromString(FString::FromInt(ProjectilesAmount)));
	}
}

void UMainWidget::UpdateScores(int Scores)
{
	if (TXTCurrentScores)
	{
		TXTCurrentScores->SetText(FText::FromString(FString::FromInt(Scores)));
	}
}

void UMainWidget::UpdateWaves(int WaveCount)
{
	if (TXTCurrentWave)
	{
		TXTCurrentWave->SetText(FText::FromString(FString::FromInt(WaveCount)));
	}
}



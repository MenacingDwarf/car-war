// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "MainHUD.generated.h"

/**
 * 
 */
UCLASS()
class CARWAR_API AMainHUD : public AHUD
{
	GENERATED_BODY()
	
public:
	AMainHUD();

	virtual void DrawHUD() override;

	virtual void BeginPlay() override;

	virtual void Tick(float DeltaSeconds) override;

	UFUNCTION()
	void StartGame(int ProjectilesAmount);

	UFUNCTION()
	void UpdateProjectiles(int ProjectilesAmount);

	UFUNCTION()
	void UpdateScores(int Scores);

	UFUNCTION()
	void UpdateWaves(int WaveCount);

	UFUNCTION()
	void GameOver();

	UPROPERTY(EditDefaultsOnly, Category = "Widgets")
	TSubclassOf<class UMainWidget> MainWidgetClass;

	UPROPERTY(EditDefaultsOnly, Category = "Widgets")
	TSubclassOf<class UGameOverWidget> GameOverWidgetClass;

private:
	class UMainWidget* MainWidget;

	class UGameOverWidget* GameOverWidget;


};

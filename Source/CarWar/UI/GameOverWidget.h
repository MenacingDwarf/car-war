// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Runtime/UMG/Public/UMG.h"
#include "GameOverWidget.generated.h"

/**
 * 
 */
UCLASS()
class CARWAR_API UGameOverWidget : public UUserWidget
{
	GENERATED_BODY()
		
	UGameOverWidget(const FObjectInitializer& ObjectInitializer);

	virtual void NativeConstruct() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget, AllowPrivateAccess = "true"))
	UTextBlock* TXTTotalWavesCount;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget, AllowPrivateAccess = "true"))
	UTextBlock* TXTTotalScores;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget, AllowPrivateAccess = "true"))
	UButton* RestartButton;

	UFUNCTION()
	void RestartGame();

public:
	UFUNCTION(BlueprintCallable)
	void UpdateGameOverInfo(int WavesCount, int Scores);
	
};

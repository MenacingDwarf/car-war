// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Runtime/UMG/Public/UMG.h"
#include "MainWidget.generated.h"

/**
 * 
 */
UCLASS()
class CARWAR_API UMainWidget : public UUserWidget
{
	GENERATED_BODY()

	UMainWidget(const FObjectInitializer& ObjectInitializer);

	virtual void NativeConstruct() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget, AllowPrivateAccess = "true"))
	UTextBlock* TXTCurrentProjectiles;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget, AllowPrivateAccess = "true"))
	UTextBlock* TXTCurrentScores;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget, AllowPrivateAccess = "true"))
	UTextBlock* TXTCurrentWave;

public:
	void UpdateProjectiles(int ProjectilesAmount);

	void UpdateScores(int Scores);

	void UpdateWaves(int WaveCount);
};

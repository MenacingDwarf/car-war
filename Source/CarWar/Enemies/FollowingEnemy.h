// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MasterEnemy.h"
#include "FollowingEnemy.generated.h"

/**
 * 
 */
UCLASS()
class CARWAR_API AFollowingEnemy : public AMasterEnemy
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* StaticMesh;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* WheelMesh1;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* WheelMesh2;

	class AHovercraftPawn* PlayerRef;
public:
	AFollowingEnemy();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void Move(float DeltaSeconds) override;

	virtual void RotateWheels(float DeltaSeconds);

	UFUNCTION()
	void HandleBeginOverlap(UPrimitiveComponent * OverlapComponent, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);
	
	void RecountDestination();

	// Called every frame
	virtual void Tick(float DeltaTime) override;
};

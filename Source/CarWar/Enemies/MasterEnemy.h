// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MasterEnemy.generated.h"

UCLASS()
class CARWAR_API AMasterEnemy : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMasterEnemy();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	float Lives;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	float Speed;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	float RewardScores;

	virtual void StartMovement(FVector NewDestination);

	virtual void GetDamage(float Damage, class AHovercraftPawn* PlayerPawn);

	virtual void EnemyDestroying();

protected:
	FVector Direction;

	FVector Destination;

	virtual void Move(float DeltaSeconds);

	UPROPERTY(EditDefaultsOnly)
	UParticleSystem* ExplosionTemplate;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf <class UCameraShake> CameraShakeClass;

	UPROPERTY(EditDefaultsOnly)
	USoundBase* ExplotionSoundClass;

	virtual void MakeExplotion();

};

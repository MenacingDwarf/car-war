// Fill out your copyright notice in the Description page of Project Settings.


#include "SimpleVehicle.h"
#include "Components/BoxComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/Engine.h"
#include "GameFramework/PlayerController.h"
#include "Engine/Engine.h"
#include "TimerManager.h"
#include "../Character/HovercraftPawn.h"


// Sets default values
ASimpleVehicle::ASimpleVehicle()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	StaticMesh->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	StaticMesh->SetCollisionResponseToAllChannels(ECR_Overlap);
	StaticMesh->OnComponentBeginOverlap.AddDynamic(this, &ASimpleVehicle::HandleBeginOverlap);
	RootComponent = StaticMesh;

	WheelMesh1 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("WheelMesh1"));
	WheelMesh1->SetupAttachment(RootComponent);

	WheelMesh2 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("WheelMesh2"));
	WheelMesh2->SetupAttachment(RootComponent);

	Destination = {-290.f, -470.f, 200.f};

}

// Called when the game starts or when spawned
void ASimpleVehicle::BeginPlay()
{
	Super::BeginPlay();
	GetWorld()->GetTimerManager().SetTimer(LifeDurationTimer, this, &ASimpleVehicle::DestroyAfterTime, 10.f, false);
}

void ASimpleVehicle::Move(float DeltaSeconds)
{
	FVector DeltaLocation = DeltaSeconds * Speed * Direction;
	AddActorWorldOffset(DeltaLocation);
}

// Called every frame
void ASimpleVehicle::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move(DeltaTime);
}

void ASimpleVehicle::DestroyAfterTime()
{
	Destroy();
}

void ASimpleVehicle::HandleBeginOverlap(UPrimitiveComponent * OverlapComponent, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	if (OtherActor != this) {
		AHovercraftPawn* PlayerPawn = Cast<AHovercraftPawn>(OtherActor);
		if (PlayerPawn)
		{
			PlayerPawn->ReceiveDamage();
		}
		else {
			AMasterEnemy* Enemy = Cast<AMasterEnemy>(OtherActor);
			if (Enemy) {
				Enemy->EnemyDestroying();
				EnemyDestroying();
			}
		}
	}
}
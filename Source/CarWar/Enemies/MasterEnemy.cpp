// Fill out your copyright notice in the Description page of Project Settings.


#include "MasterEnemy.h"
#include "../Character/HovercraftPawn.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystem.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/PlayerCameraManager.h"
#include "Sound/SoundBase.h"


// Sets default values
AMasterEnemy::AMasterEnemy()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AMasterEnemy::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMasterEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AMasterEnemy::StartMovement(FVector NewDestination)
{
	Destination = NewDestination;
	// Change actor rotation to destination
	FRotator NewRotation = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), Destination);
	SetActorRotation(FRotator(GetActorRotation().Pitch, NewRotation.Yaw, GetActorRotation().Roll));
	// Direction depends on rotation
	Direction = UKismetMathLibrary::GetForwardVector(GetActorRotation());
}

void AMasterEnemy::GetDamage(float Damage, AHovercraftPawn* PlayerCharacter)
{
	Lives -= Damage;
	if (Lives <= 0)
	{
		// If lives has ended player should receive scores and enemy should be destroyed
		if (PlayerCharacter) {
			PlayerCharacter->ReceiveScores(RewardScores);
		}
		EnemyDestroying();
	}
}

void AMasterEnemy::Move(float DeltaSeconds)
{
}

void AMasterEnemy::MakeExplotion()
{
	// Play sound, particles and camera shake
	if (ExplotionSoundClass && ExplosionTemplate && CameraShakeClass) {
		UGameplayStatics::PlaySound2D(GetWorld(), ExplotionSoundClass);
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ExplosionTemplate, FTransform(GetActorLocation()), true, EPSCPoolMethod::None, true);
		GetWorld()->GetFirstPlayerController()->PlayerCameraManager->PlayCameraShake(CameraShakeClass, 1.f);
	}
}

void AMasterEnemy::EnemyDestroying()
{
	MakeExplotion();
	Destroy();
}


// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MasterEnemy.h"
#include "SimpleVehicle.generated.h"

UCLASS()
class CARWAR_API ASimpleVehicle : public AMasterEnemy
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* StaticMesh;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* WheelMesh1;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* WheelMesh2;
	
public:	
	// Sets default values for this actor's properties
	ASimpleVehicle();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	virtual void Move(float DeltaSeconds) override;

	FTimerHandle LifeDurationTimer;

	void DestroyAfterTime();

	UFUNCTION()
	void HandleBeginOverlap(UPrimitiveComponent * OverlapComponent, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};

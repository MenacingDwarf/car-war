// Fill out your copyright notice in the Description page of Project Settings.


#include "FollowingEnemy.h"
#include "GameFramework/PlayerController.h"
#include "Components/StaticMeshComponent.h"
#include "Kismet/KismetMathLibrary.h"

#include "../Character/HovercraftPawn.h"

AFollowingEnemy::AFollowingEnemy()
{
	PrimaryActorTick.bCanEverTick = true;

	// Create body mesh with overlap event
	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	StaticMesh->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	StaticMesh->SetCollisionResponseToAllChannels(ECR_Overlap);
	StaticMesh->OnComponentBeginOverlap.AddDynamic(this, &AFollowingEnemy::HandleBeginOverlap);
	RootComponent = StaticMesh;

	// Create wheels that should be rotated
	WheelMesh1 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("WheelMesh1"));
	WheelMesh1->SetupAttachment(RootComponent);
	WheelMesh2 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("WheelMesh2"));
	WheelMesh2->SetupAttachment(RootComponent);

	Destination = { -290.f, -470.f, 200.f };
}

void AFollowingEnemy::BeginPlay()
{
	Super::BeginPlay();
	// At the beggining we should get player ref for following
	PlayerRef = Cast<AHovercraftPawn>(GetWorld()->GetFirstPlayerController()->GetPawn());
}

void AFollowingEnemy::Move(float DeltaSeconds)
{
	// During the moving new destination are counting, location of actor is changing and wheels are rotating
	RecountDestination();
	FVector DeltaLocation = DeltaSeconds * Speed * Direction;
	AddActorWorldOffset(DeltaLocation);
	RotateWheels(DeltaSeconds);
}

void AFollowingEnemy::RotateWheels(float DeltaSeconds)
{
	WheelMesh1->AddLocalRotation(FRotator(-260.f, 0.f, 0.f * DeltaSeconds));
	WheelMesh2->AddLocalRotation(FRotator(-260.f, 0.f, 0.f * DeltaSeconds));
}

void AFollowingEnemy::HandleBeginOverlap(UPrimitiveComponent * OverlapComponent, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	// Check not overlapping itself
	if (OtherActor != this) {
		// if overlapping player this player should receive damage
		AHovercraftPawn* PlayerPawn = Cast<AHovercraftPawn>(OtherActor);
		if (PlayerPawn)
		{
			PlayerPawn->ReceiveDamage();
		}
		else {
			// if overlapping another enemy both of enemies should be destroyed
			AMasterEnemy* Enemy = Cast<AMasterEnemy>(OtherActor);
			if (Enemy) {
				Enemy->EnemyDestroying();
				EnemyDestroying();
			}
		}
	}
}

void AFollowingEnemy::RecountDestination()
{
	// To recount destination to player standart enemy function for starting movement is called
	// During this function work actor rotation and direction will be changed
	StartMovement(PlayerRef->GetActorLocation());
}

void AFollowingEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move(DeltaTime);
}

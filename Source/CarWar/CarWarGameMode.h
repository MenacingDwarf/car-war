// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "CarWarGameMode.generated.h"

UCLASS(minimalapi)
class ACarWarGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ACarWarGameMode();

protected:

	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly)
	USoundBase* NoiseSoundClass;

	UPROPERTY(EditDefaultsOnly)
	USoundBase* BackgroundMusicSoundClass;
};




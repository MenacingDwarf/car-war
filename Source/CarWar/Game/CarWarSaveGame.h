// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "CarWarSaveGame.generated.h"

/**
 * 
 */
UCLASS()
class CARWAR_API UCarWarSaveGame : public USaveGame
{
	GENERATED_BODY()
		
public:

	UCarWarSaveGame();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int WavesCount;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int Scores;
	
};

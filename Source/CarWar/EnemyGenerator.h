// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "EnemyGenerator.generated.h"

USTRUCT(BlueprintType)
struct FSpawnPoint
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int WallIndex;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Meta = (MakeEditWidget = true))
	FVector PointLocation;
};

USTRUCT(BlueprintType)
struct FSpawnEnemyDesc
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int SpawnWeight;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<class AMasterEnemy> EnemyClass;
};

UCLASS()
class CARWAR_API AEnemyGenerator : public AActor
{
	GENERATED_BODY()
	
	class AMainHUD* MainHUDRef;
	
public:	
	// Sets default values for this actor's properties
	AEnemyGenerator();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int WallsAmount;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FSpawnPoint> SpawnPoints;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FSpawnEnemyDesc> EnemyClasses;

	FTimerHandle EnemySpawnTimer;

	float SpawnProbability;

	float MaxSpawnProbability;

	float SpawnTime;

	float MinSpawnTime;

	int WaveCount;

	void SpawnEnemies();
	
	void SpawnSingleEnemy(FSpawnPoint SpawnPoint);

	FVector GetRandomDestination(int ExcludedWallIndex);

	FVector GetRandomPointOnWall(int WallIndex);

	TSubclassOf<class AMasterEnemy> GetRandomEnemyClass();

	void UpdateWavesCount();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};

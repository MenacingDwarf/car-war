// Fill out your copyright notice in the Description page of Project Settings.


#include "HovercraftPawn.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "GameFramework/PlayerController.h"
#include "Kismet/GameplayStatics.h"
#include "TimerManager.h"
#include "Engine/Engine.h"
#include "../UI/MainHUD.h"
#include "../Enemies/MasterEnemy.h"
#include "../Game/CarWarSaveGame.h"
#include "GameFramework/PlayerState.h"
#include "HovercraftPlayerState.h"
#include "Projectile.h"

// Sets default values
AHovercraftPawn::AHovercraftPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Set movement speed parameters
	MoveSpeed = 500.f;
	TurnSpeed = 45.f;

	// At start of game we have 30 projectiles, can fire but can't play while cutscene
	ProjectilesAmount = 30;
	bCanFire = true;
	bCanPlay = false;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Box Collision settings
	BoxCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxComponent"));
	BoxCollision->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	BoxCollision->SetCollisionResponseToAllChannels(ECR_Overlap);
	BoxCollision->SetBoxExtent(FVector(150.f, 70.f, 30.f));
	RootComponent = BoxCollision;

	// Create meshes for player  body and player weapon
	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	StaticMesh->SetupAttachment(RootComponent);
	WeaponMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("WeaponMesh"));
	WeaponMesh->SetupAttachment(StaticMesh);

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 1200.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false;	
}

// Called when the game starts or when spawned
void AHovercraftPawn::BeginPlay()
{
	Super::BeginPlay();
	// Save MainHudRef for UI displaying
	MainHUDRef = Cast<AMainHUD>(GetWorld()->GetFirstPlayerController()->GetHUD());

	//Timer for bullets receiving
	GetWorld()->GetTimerManager().SetTimer(ProjectileReloadHandle, this, &AHovercraftPawn::ReceiveProjectilesOnTime, 2.5f, false);

}

// Called every frame
void AHovercraftPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AHovercraftPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward", this, &AHovercraftPawn::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AHovercraftPawn::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &AHovercraftPawn::Turn);
	PlayerInputComponent->BindAxis("TurnRate", this, &AHovercraftPawn::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AHovercraftPawn::LookUpAtRate);

	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &AHovercraftPawn::FireHandler);

}

void AHovercraftPawn::ReceiveProjectilesOnTime()
{
	ReceiveProjectiles();
	GetWorld()->GetTimerManager().SetTimer(ProjectileReloadHandle, this, &AHovercraftPawn::ReceiveProjectilesOnTime, 2.5f, false);
}

void AHovercraftPawn::ResetCanFire()
{
	bCanFire = true;
}

void AHovercraftPawn::GameOver()
{
	// Pause game and switch into UI mode
	APlayerController* PC = Cast<APlayerController>(GetController());
	PC->SetPause(true);
	PC->SetInputMode(FInputModeUIOnly());
	PC->bShowMouseCursor = true;

	// We should save our results if it is new record
	SaveIfRecord();

	// Show game over widget
	MainHUDRef->GameOver();
}

void AHovercraftPawn::SaveIfRecord()
{
	// Get refs to Player State and Save Game
	AHovercraftPlayerState *HovercraftPlayerState = GetWorld()->GetFirstPlayerController()->GetPlayerState<AHovercraftPlayerState>();
	UCarWarSaveGame* SaveGameInstance = Cast<UCarWarSaveGame>(UGameplayStatics::LoadGameFromSlot("Record", 0));

	if (SaveGameInstance)
	{
		// If scores is record it should be saved
		if (SaveGameInstance->Scores < HovercraftPlayerState->Scores)
		{
			SaveGameInstance->Scores = HovercraftPlayerState->Scores;
		}

		// If waves is record it should be saved
		if (SaveGameInstance->WavesCount < HovercraftPlayerState->WavesCount)
		{
			SaveGameInstance->WavesCount = HovercraftPlayerState->WavesCount;
		}
	}
	else // Create new Save Game if it is the first game
	{
		SaveGameInstance = Cast<UCarWarSaveGame>(UGameplayStatics::CreateSaveGameObject(UCarWarSaveGame::StaticClass()));
		SaveGameInstance->Scores = HovercraftPlayerState->Scores;
		SaveGameInstance->WavesCount = HovercraftPlayerState->WavesCount;
	}

	// Save changed (or not) Save Game
	UGameplayStatics::SaveGameToSlot(SaveGameInstance, TEXT("Record"), 0);
}

void AHovercraftPawn::ReceiveProjectiles(int ReceivableProjectilesAmount)
{
	// Receive new projectiles and update widget
	ProjectilesAmount += ReceivableProjectilesAmount;
	if (MainHUDRef) {
		MainHUDRef->UpdateProjectiles(ProjectilesAmount);
	}
}

void AHovercraftPawn::ReceiveScores(int ReceivableScores)
{
	// Receive new scores in Player State and update widget
	AHovercraftPlayerState *HovercraftPlayerState = GetWorld()->GetFirstPlayerController()->GetPlayerState<AHovercraftPlayerState>();
	if (HovercraftPlayerState)
	{
		HovercraftPlayerState->Scores += ReceivableScores;
		MainHUDRef->UpdateScores(HovercraftPlayerState->Scores);
	}
}

void AHovercraftPawn::ReceiveDamage(int ReceivableDamage)
{
	// At this moment receiving damage = Game Over
	GameOver();
}

void AHovercraftPawn::TurnAtRate(float Rate)
{
	// Mouse Rotation and weapon rotation if cutscene is ended
	if (bCanPlay)
	{
		// calculate delta for this frame from the rate information
		float DeltaRotation = Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds();
		AddControllerYawInput(DeltaRotation);
		WeaponMesh->AddWorldRotation(FRotator(0.f, DeltaRotation, 0.f));
	}
}

void AHovercraftPawn::Turn(float Rate)
{
	// Mouse Rotation and weapon rotation if cutscene is ended
	if (bCanPlay)
	{
		AddControllerYawInput(Rate);
		WeaponMesh->AddWorldRotation(FRotator(0.f, Rate * 2.5f, 0.f));
	}
}

void AHovercraftPawn::LookUp(float Rate)
{
	if (bCanPlay)
	{
		APawn::AddControllerPitchInput(Rate);
	}
}

void AHovercraftPawn::LookUpAtRate(float Rate)
{
	if (bCanPlay)
	{
		// calculate delta for this frame from the rate information
		AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
	}
}

void AHovercraftPawn::FireHandler()
{
	// Player can fire only if he has projectiles, not reloading and cutscene is ended
	if (ProjectilesAmount > 0 && bCanFire && bCanPlay) {
		// Create new projectile on scene
		SpawnProjectile();

		// Play fire effects
		if (ShootingSoundClass && CameraShakeClass) {
			UGameplayStatics::PlaySound2D(GetWorld(), ShootingSoundClass);
			GetWorld()->GetFirstPlayerController()->PlayerCameraManager->PlayCameraShake(CameraShakeClass, 1.f);
		}

		// Spend projectile and update widget
		ProjectilesAmount--;
		MainHUDRef->UpdateProjectiles(ProjectilesAmount);

		// Now reloading should started
		bCanFire = false;
		GetWorld()->GetTimerManager().SetTimer(FireAvailabilityHandle, this, &AHovercraftPawn::ResetCanFire, 0.5f, false);
	}
}

void AHovercraftPawn::SpawnProjectile()
{
	// Calculate new projectile location and rotation according to weapon location and rotation
	FVector WeaponLocation = WeaponMesh->GetComponentLocation();
	FRotator WeaponRotation = WeaponMesh->GetComponentRotation();
	FVector SpawnLocation = WeaponLocation + 50 * WeaponRotation.Vector();

	// Spawn new projectile and set player ref
	AProjectile* NewProjectile = GetWorld()->SpawnActor<AProjectile>(ProjectileClass, FTransform(WeaponRotation, SpawnLocation));
	if (NewProjectile) {
		NewProjectile->PlayerRef = this;
	}
}

void AHovercraftPawn::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f) && bCanPlay)
	{
		// find out which way is forward
		const FRotator Rotation = GetActorRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);

		AddActorWorldOffset(Direction * Value * MoveSpeed * GetWorld()->GetDeltaSeconds() , true);
	}
}

void AHovercraftPawn::MoveRight(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f) && bCanPlay)
	{
		// Rotate actor by TurnSpeed, but weapon must not be rotated so rotate this in opposite side
		AddActorWorldRotation(FRotator(0.f, Value * TurnSpeed * GetWorld()->GetDeltaSeconds(), 0.f));
		WeaponMesh->AddWorldRotation(FRotator(0.f, -Value * TurnSpeed * GetWorld()->GetDeltaSeconds(), 0.f));
	}
}


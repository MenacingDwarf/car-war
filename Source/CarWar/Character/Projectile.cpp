// Fill out your copyright notice in the Description page of Project Settings.


#include "Projectile.h"
#include "Kismet/KismetMathLibrary.h"
#include "Components/StaticMeshComponent.h"
#include "HovercraftPawn.h"
#include "Engine/Engine.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystem.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/PlayerCameraManager.h"
#include "Sound/SoundBase.h"
#include "TimerManager.h"

#include "../Enemies/MasterEnemy.h"

// Sets default values
AProjectile::AProjectile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Create mesh with overlap event
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
	MeshComponent->OnComponentBeginOverlap.AddDynamic(this, &AProjectile::HandleBeginOverlap);

	Speed = 1000.f;
	Damage = 1.f;

}

// Called when the game starts or when spawned
void AProjectile::BeginPlay()
{
	Super::BeginPlay();
	// Set direction into rotation and start destroying after 5 secs
	Direction = GetActorRotation().Vector();
	GetWorld()->GetTimerManager().SetTimer(LifeDurationTimer, this, &AProjectile::DestroyAfterTime, 5.f, false);
}

// Called every frame
void AProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move(DeltaTime);
}

void AProjectile::Move(float DeltaTime)
{
	FVector DeltaLocation = DeltaTime * Speed * Direction;
	AddActorWorldOffset(DeltaLocation);
}

void AProjectile::DestroyAfterTime()
{
	Destroy();
}

void AProjectile::DestroyProjectile()
{
	MakeExplotion();
	Destroy();
}

void AProjectile::HandleBeginOverlap(UPrimitiveComponent * OverlapComponent, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	// If projectile overlapped enemy this enemy should get damage and projectile should be destroyed
	AMasterEnemy* OverlappedEnemy = Cast<AMasterEnemy>(OtherActor);
	if (OverlappedEnemy)
	{
		OverlappedEnemy->GetDamage(Damage, PlayerRef);
		DestroyProjectile();
	}
}

void AProjectile::MakeExplotion()
{
	// Play sound, particles and camera shake
	if (ExplotionSoundClass && ExplosionTemplate && CameraShakeClass) {
		UGameplayStatics::PlaySound2D(GetWorld(), ExplotionSoundClass);
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ExplosionTemplate, FTransform(GetActorLocation()), true, EPSCPoolMethod::None, true);
		GetWorld()->GetFirstPlayerController()->PlayerCameraManager->PlayCameraShake(CameraShakeClass, 1.f);
	}
}


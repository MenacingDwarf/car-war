// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "HovercraftPawn.generated.h"

UCLASS()
class CARWAR_API AHovercraftPawn : public APawn
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* StaticMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* WeaponMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UBoxComponent* BoxCollision;


public:
	// Sets default values for this pawn's properties
	AHovercraftPawn();

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	float BaseLookUpRate;

	class AMainHUD* MainHUDRef;

	UPROPERTY(BlueprintReadWrite)
	bool bCanPlay;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void MoveForward(float Value);

	void MoveRight(float Value);

	void TurnAtRate(float Rate);

	void Turn(float Rate);

	void LookUp(float Rate);

	void LookUpAtRate(float Rate);

	void FireHandler();

	void SpawnProjectile();

	float MoveSpeed;
	float TurnSpeed;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }

protected:
	UPROPERTY(BlueprintReadWrite)
	int ProjectilesAmount;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<class AProjectile> ProjectileClass;

	UPROPERTY(EditDefaultsOnly)
	USoundBase* ShootingSoundClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf <class UCameraShake> CameraShakeClass;

	FTimerHandle ProjectileReloadHandle;

	bool bCanFire;

	void ReceiveProjectilesOnTime();

	FTimerHandle FireAvailabilityHandle;
	void ResetCanFire();

	void GameOver();

	void SaveIfRecord();

public:
	UFUNCTION(BlueprintCallable)
	void ReceiveProjectiles(int ReceivableProjectilesAmount = 1);

	UFUNCTION(BlueprintCallable)
	void ReceiveScores(int ReceivableScores = 1);

	UFUNCTION(BlueprintCallable)
	void ReceiveDamage(int ReceivableDamage = 1);
};

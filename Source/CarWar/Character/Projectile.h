// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Projectile.generated.h"

UCLASS()
class CARWAR_API AProjectile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AProjectile();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UStaticMeshComponent* MeshComponent;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	float Speed;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	float Damage;

	FVector Direction;
	class AHovercraftPawn* PlayerRef;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void Move(float DeltaTime);

	FTimerHandle LifeDurationTimer;

	void DestroyAfterTime();

	void DestroyProjectile();

	UFUNCTION()
	void HandleBeginOverlap(UPrimitiveComponent * OverlapComponent, AActor * OtherActor,
			UPrimitiveComponent * OtherComp, int32 OtherBodyIndex,
			bool bFromSweep, const FHitResult & SweepResult);

protected:
	UPROPERTY(EditDefaultsOnly)
	UParticleSystem* ExplosionTemplate;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf <class UCameraShake> CameraShakeClass;

	UPROPERTY(EditDefaultsOnly)
	USoundBase* ExplotionSoundClass;

	virtual void MakeExplotion();

};

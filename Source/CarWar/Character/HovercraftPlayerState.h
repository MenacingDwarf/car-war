// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "HovercraftPlayerState.generated.h"

/**
 * 
 */
UCLASS()
class CARWAR_API AHovercraftPlayerState : public APlayerState
{
	GENERATED_BODY()
public:
	AHovercraftPlayerState();

	int Scores;

	int WavesCount;
	
};
